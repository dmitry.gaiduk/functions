const getSum = (str1, str2) => {
  if (isNaN(+str1) || typeof str1 === 'number' || typeof str1 === 'object') {
    return false
  }
  if (isNaN(+str2) || typeof str2 === 'number' || typeof str2 === 'object') {
    return false
  }
  return (+str1 + +str2).toString()
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;

  listOfPosts.forEach(el => {
    if (el.author === authorName) {
      posts++
    }
    if (el.comments) {
      el.comments.forEach(comment => {
        if (comment.author === authorName) {
          comments++
        }
      })
    }
  });
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
    let cashbox = {
        '25usd':0,
        '50usd':0,
        '100usd':0
    }

    for(let i=0; i< people.length; i++){
        
        if(+people[i]===25) {
            cashbox['25usd']++;
        }

        if (+people[i]===50){
            cashbox['50usd']++;
            if (cashbox['25usd'] ){
                cashbox['25usd']--
            } else {
                return 'NO'
            } 
        }

        if (+people[i]===100){
            cashbox['100usd']++;
            //two ways to give charge
            // 25+25+25 && 50+25
            if (cashbox['25usd']>=1 && cashbox['50usd']>=1) {
                cashbox['25usd']--;
                cashbox['50usd']--; 
            } else if(cashbox['25usd'] >= 3) {
                cashbox['25usd'] -=3
            }  else {
                return "NO"
            }
        }
    }
  return "YES"
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
